// # TEMPLATE
( function MyApp(){
  // console.log('init function');
  // https://www.json-generator.com/#
  var fetchedData = fetch('http://www.json-generator.com/api/json/get/bUuBhzcCRe?indent=2').then(function(response) {
    return response.json();
  })
  .then(data => {
    renderInterface(data);
    // console.log(data);



  });
}());




// Задача:
// Написать ф-ю которая принимает на вход обьект с сервера и
// разбить его на 3 массива по параметрам описаным ниже.
// + бонус вывести каждый список на экран
// + бонус 2 сделать поле инпута куда вставить ссылку с json-generator
// для перерендера списка по клику на кнопку
// + бонус 3 если вставить не валидную ссылку выводить ошибку

// # = условие -> вывод
// array 1 = fruit == banana -> name,
// array 2 = balance > 2000, age > 25
// array 3 = eyeColor === blue, gender === female, isActive === false

function renderInterface( data ){

    var originArray = data.map((elem) =>
     elem
    );
  
    var array1 = data.filter((elem) =>
    elem.favoriteFruit === "banana" ).map((elem) =>
    elem.name + ' like ' + elem.favoriteFruit
    );  
  
    var array2 = data.filter((elem) =>
    elem.age > 25 && Number.parseInt(elem.balance.replace(/\$|\,/gi, '')) > 2000
    );
  
    var array3 = data.filter((elem) =>
    elem.eyeColor === "blue" && elem.gender === "female" && elem.isActive === false
   );

   
  
    console.log("origin_array", originArray);
    console.log('array1', array1);
    console.log( 'array2',array2);
    console.log( 'array3',array3);
    

  
  }